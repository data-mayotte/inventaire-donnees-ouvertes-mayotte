const fs = require('fs');
const path = require('path');

const sourceFile = path.join(__dirname, '../../api/usgs-mayotte-2018.geojson');
const destFile = path.join(__dirname, '../../api/usgs-mayotte-2018.ext.geojson');

console.log('');
console.log('Source file: ', sourceFile)
const json = JSON.parse(fs.readFileSync(sourceFile, 'utf8'));

// console.log(JSON.stringify(json, true, 2));

const newJson = {
    ...json,
    features: json.features.map(feature => {
        // console.log(feature.properties);
        // console.log('Input time:', feature.properties.time);
        // console.log('Output time:', new Date(feature.properties.time).toISOString());
        return {
            ...feature,
            properties: {
                ...feature.properties,
                timeString: new Date(feature.properties.time).toISOString(),
            },
        };
    }),
};

fs.writeFileSync(destFile, JSON.stringify(newJson, true, 2), 'utf8');

console.log('Generated file: ', destFile)
console.log('');
