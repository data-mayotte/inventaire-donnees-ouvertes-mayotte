#!/bin/bash

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
USGS_DIR="$( cd "$( dirname "${BASE_DIR}" )" && pwd )"

API_DATA_DIR=$USGS_DIR/api
mkdir -p $API_DATA_DIR

URL="https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2018-01-01&endtime=2018-12-31&minlatitude=-13.3&minlongitude=44.7&maxlatitude=-12.3&maxlongitude=45.5"
curl -o "$API_DATA_DIR/usgs-mayotte-2018.geojson" $URL
(cd ./usgs-data-converter && npm start)