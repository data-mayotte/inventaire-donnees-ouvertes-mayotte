# Téléchargement des données de l'USGS

## Description

Les données de l'USGS ne sont pas stockées dans le projet afin de ne pas le surcharger.

Ce script permet de les télécharger grace  à l'API de l'USGS.

Ce script fonctionne sous linux et mac.

## Pré-requis

Installer [nodejs](https://nodejs.org).

## Utilisation

Pour mettre à jour toutes les données:

```bash
./download-usgs-data.sh
```

2 fichiers sont générés:

- /data/usgs/api/usgs-mayotte-2018.geojson: le fichier original
- /data/usgs/api/usgs-mayotte-2018.ext.geojson: le fichier transformé afin d'ajouter le champ timeString, calculé à partir du champ time

## Licence

![Public Domain](https://i.creativecommons.org/p/zero/1.0/88x31.png "Public Domain").

Ces scripts sont libres de droits (licence [CC0 1.0 universel (CC0 1.0)](https://creativecommons.org/publicdomain/zero/1.0/deed.fr)).