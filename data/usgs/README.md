
# USGS (United States Geological Survey) - données sur Mayotte

Institut d'études géologiques des États-Unis

[https://earthquake.usgs.gov/data/](https://earthquake.usgs.gov/data/)

## Projets QGIS exemples

Voir [projets QGIS USGS](../../qgis/provider/usgs/README.md)

## USGS Explorer

[https://earthexplorer.usgs.gov/](https://earthexplorer.usgs.gov/)

Tutoriel [en]: [https://gisgeography.com/usgs-earth-explorer-download-free-landsat-imagery/](https://gisgeography.com/usgs-earth-explorer-download-free-landsat-imagery/)

__Note__: le téléchargement des données nécessite de créer un compte (gratuit).

![Carte](./images/usgs-earthexplorer-mayotte.png)

### Modèle Numérique de Terrain (Digital Elevation Model)

| **Donnée**               | **Lien**                                                                                                                                                       | **Description**               |
| ------------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------- |
| ASTER GLOBAL DEM         | [documentation USGS](https://lpdaac.usgs.gov/dataset_discovery/aster/aster_products_table/astgtm) [documentation NASA](https://asterweb.jpl.nasa.gov/gdem.asp) | 1 arc-second (10-25m), 2011   |
| GMTED2010                | [documentation USGS](https://lta.cr.usgs.gov/GMTED2010)                                                                                                        | 7.5-arc-second (25-42m), 2010 |
| GTOPO30                  | [documentation USGS](https://lta.cr.usgs.gov/GTOPO30)                                                                                                          | 30 arc seconds (1km)          |
| SRTM 1 Arc-Second Global | [documentation USGS](https://lta.cr.usgs.gov/SRTM1Arc)    [documentation NASA](https://www2.jpl.nasa.gov/srtm/)                                                | 1 arc-second (30m), 2000      |
| SRTM Non-Void Filled     | [documentation USGS](https://lta.cr.usgs.gov/SRTMNVF)    [documentation NASA](https://www2.jpl.nasa.gov/srtm/)                                                 | 1 arc-second (30m), 2000      |
| SRTM Void Filled         | [documentation USGS](https://lta.cr.usgs.gov/SRTMVF)    [documentation NASA](https://www2.jpl.nasa.gov/srtm/)                                                  | 1 arc-second (30m), 2000      |
| SRTM Water Body          | [documentation USGS](https://lta.cr.usgs.gov/srtm_water_body_dataset)    [documentation NASA](https://www2.jpl.nasa.gov/srtm/)                                 | 1 arc-second (30m), 2000      |

![Carte](./images/usgs-aster-global-dem-mayotte.png)

## Données sismiques

![Carte](./images/usgs-seismes-mayotte-2018.png)

Documentation:

- API: [https://earthquake.usgs.gov/fdsnws/event/1/](https://earthquake.usgs.gov/fdsnws/event/1/)
- Feeds: [https://earthquake.usgs.gov/earthquakes/feed/v1.0/geojson.php](https://earthquake.usgs.gov/earthquakes/feed/v1.0/geojson.php)

Liens:

- [https://fr.wikipedia.org/wiki/Institut_d'%C3%A9tudes_g%C3%A9ologiques_des_%C3%89tats-Unis](https://fr.wikipedia.org/wiki/Institut_d'%C3%A9tudes_g%C3%A9ologiques_des_%C3%89tats-Unis)
- [USGS: GSN and ANSS Stations](https://earthquake.usgs.gov/monitoring/operations/network.php)
- [Liste des stations de International Registry of Seismograph Stations](http://www.isc.ac.uk/cgi-bin/stations?sta_list=&stn_ctr_lat=&stn_ctr_lon=&stn_radius=&max_stn_dist_units=deg&stnsearch=RECT&stn_bot_lat=-15&stn_top_lat=-10&stn_left_lon=47&stn_right_lon=43&stn_srn=&stn_grn=)

Exemple de requête personnalisée:

[Tous les séismes autour de Mayotte en 2018](https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2018-01-01&endtime=2018-12-31&minlatitude=-13.3&minlongitude=44.7&maxlatitude=-12.3&maxlongitude=45.5)

Exemple de feed:

- [Séismes > 4.5 depuis 1 mois dans le monde](https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/4.5_month.geojson)

## Mesures

L'USGS ne possède pas de stations de mesures à proximité de Mayotte.

La précision de ses mesures est donc limitée, ce qui fait que seuls les séismes les plus importants sont détectés, et l'évaluation de leur intensité peut varier des estimations faites par le BRGM à Mayotte.

![Carte](./images/usgs-stations-proximite-mayotte.png)
