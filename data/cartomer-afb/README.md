# CartoMer (Agence française pour la biodiversité) - données sur Mayotte

[http://cartographie.aires-marines.fr](http://cartographie.aires-marines.fr)
[https://www.afbiodiversite.fr](https://www.afbiodiversite.fr)

[Services WMS, WFS et CSW](http://cartographie.aires-marines.fr/?q=node/15)

![Carte](./images/screenshot/cartomer-parc-provincial.png "CartoMer -Parc provincial - Mayotte")
![Carte](./images/screenshot/cartomer-zones.png "CartoMer - Zones des vocations définies par le plan de gestion des parcs naturels marins - Mayotte")

## CartoMer - Aires marines

Service WFS: [http://cartographie.aires-marines.fr/wfs](http://cartographie.aires-marines.fr/wfs)

|                                  **Donnée**                                   |              **Couche**              |
| ----------------------------------------------------------------------------- | ------------------------------------ |
| Parc provincial                                                               | ges_omon_amp_aamp_pol_wgs84_vue-18   |
| Zones des vocations définies par le plan de gestion des parcs naturels marins | ges_omon_vocation_pnm_aamp_pol_wgs84 |
