#!/bin/bash

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
USGS_DIR="$( cd "$( dirname "${BASE_DIR}" )" && pwd )"

WFS_DATA_DIR=$USGS_DIR/wfs
mkdir -p $WFS_DATA_DIR

(cd ./brgm-downloader && npm start)