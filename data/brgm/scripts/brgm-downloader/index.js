const fs = require('fs');
const path = require('path');
const http = require('http');

const destDir = path.join(__dirname, '../../wfs');
if (!fs.existsSync(destDir)) {
  // create output directory
  fs.mkdirSync(destDir);
}

// http://geoservices.brgm.fr/geologie?service=WFS&version=1.0.0&request=GetCapabilities


const MAYOTTE_BBOX = '44.7,-13.3,45.5,-12.3';

function downloadWfs(wfsHost, wfsContext, typeName, outputFilename, outputFormat) {

  console.log('');
  console.log(`Download "${typeName}" layer to "${outputFilename}" from url: ${wfsHost}${wfsContext}...`);

  const wfsPath = `${wfsContext}?service=WFS&version=1.0.0&request=GetFeature&typeName=${typeName}&BBOX=${MAYOTTE_BBOX}${outputFormat ? `&outputFormat=${outputFormat}` : ''}`;

  const options = {
    host: wfsHost, //'geoservices.brgm.fr',
    // path: '/geologie?service=WFS&version=1.0.0&request=GetFeature&typeName=BSS_EAU_QT_POINT&BBOX=44.7,-13.3,45.5,-12.3&outputFormat=GML3&cql_filter=id=1',
    path: wfsPath,
    method: 'GET'
  }


  const destFile = path.join(destDir, outputFilename);

  const file = fs.createWriteStream(destFile, 'utf8');

  const request = http.request(options, function (res) {
    res.pipe(file);

    res.on('data', function (chunk) {
      // console.log('data chunk: ', chunk.length)
    });
    res.on('end', function () {
      console.log('Generated file: ', destFile)
      console.log('');

    });
  });
  request.on('error', function (e) {
    console.log(e.message);
  });
  request.end();
}

// risques
downloadWfs('geoservices.brgm.fr', '/risques', 'SIS_OUTREMER', 'risques-SIS_OUTREMER-intensité-des-épicentres-en-outre-mer.gml');
downloadWfs('geoservices.brgm.fr', '/risques', 'SIS_INTENSITE', 'risques-SIS_INTENSITE-intensité-interpolée-maximale-possiblement-ressentie-par-communes.gml');

// geologie
downloadWfs('geoservices.brgm.fr', '/geologie', 'BSS_EAU_QT_POINT', 'geologie-BSS_EAU_QT_POINT-points-d-eau-avec-des-mesures-de-niveau-d-eau.gml');
downloadWfs('geoservices.brgm.fr', '/geologie', 'BSS_EAU_POINT', 'geologie-BSS EAU-BSS_EAU_POINT-référentiel-des-points-d-eau.gml');
downloadWfs('geoservices.brgm.fr', '/geologie', 'SCAN_H_GEOL50_PERIMETRE', 'geologie-SCAN_H_GEOL50_PERIMETRE.gml');
downloadWfs('geoservices.brgm.fr', '/geologie', 'BSS_SEMIS_0', 'geologie-BSS_SEMIS_0.gml');
downloadWfs('geoservices.brgm.fr', '/geologie', 'BSS_TOTAL_SANS_LABEL', 'geologie-BSS_TOTAL_SANS_LABEL.gml');
downloadWfs('geoservices.brgm.fr', '/geologie', 'BSS_TOTAL_AVEC_LABEL', 'geologie-BSS_TOTAL_AVEC_LABEL.gml');
downloadWfs('geoservices.brgm.fr', '/geologie', 'BSS_EAU_DENSITE', 'geologie-BSS_EAU_DENSITE.gml');
downloadWfs('geoservices.brgm.fr', '/geologie', 'BGM_STATION', 'geologie-BGM_STATION.gml');
downloadWfs('geoservices.brgm.fr', '/geologie', 'BGM_TYPE', 'geologie-BGM_TYPE.gml');
downloadWfs('geoservices.brgm.fr', '/geologie', 'MINES_EMP', 'geologie-MINES_EMP.gml');

// // granulats marins
downloadWfs('geoservices.brgm.fr', '/odmgm', 'EXPLOIT_ACTIVE_L', 'granulats-marins-EXPLOIT_ACTIVE_L-contours-des-exploitations-de-matériaux-en-activité.gml');
downloadWfs('geoservices.brgm.fr', '/odmgm', 'EXPLOIT_FERMEE_L', 'granulats-marins-EXPLOIT_FERMEE_L-contours-des-exploitations-de-matériaux-fermées.gml');
downloadWfs('geoservices.brgm.fr', '/odmgm', 'EXPLOIT_ACTIVE_P', 'granulats-marins-EXPLOIT_ACTIVE_P-exploitations-de-matériaux-en-activité.gml');
downloadWfs('geoservices.brgm.fr', '/odmgm', 'EXPLOIT_FERMEE_P', 'granulats-marins-EXPLOIT_FERMEE_P-exploitations-de-matériaux-fermées.gml');