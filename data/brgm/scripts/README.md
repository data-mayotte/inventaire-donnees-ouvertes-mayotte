# Téléchargement des données de l'BRGM

## Description

Les données du BRGM ne sont pas stockées dans le projet afin de ne pas le surcharger.

Ce script permet de les télécharger grace aux services WFS du BRGM.

Ce script fonctionne sous linux et mac.

## Pré-requis

Installer [nodejs](https://nodejs.org).

## Utilisation

Pour mettre à jour toutes les données:

```bash
./download-brgm-data.sh
```

Les fichiers sont générés dans:

- /data/brgm/wfs

## Licence

![Public Domain](https://i.creativecommons.org/p/zero/1.0/88x31.png "Public Domain").

Ces scripts sont libres de droits (licence [CC0 1.0 universel (CC0 1.0)](https://creativecommons.org/publicdomain/zero/1.0/deed.fr)).