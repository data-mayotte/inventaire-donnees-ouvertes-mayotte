# BRGM (Bureau de Recherches Géologiques et Minières) - données sur Mayotte

[http://www.brgm.fr/](http://www.brgm.fr/)

[Services WMS, WFS et CSW](http://infoterre.brgm.fr/page/geoservices-ogc)

## Projets QGIS exemples

Voir [projets QGIS BRGM](../../qgis/provider/brgm/README.md)

## BRGM - Risques

![Carte](./images/brgm-risques-mayotte.png)

Service WFS: [http://geoservices.brgm.fr/risques](http://geoservices.brgm.fr/risques)

| **Donnée**                                                                    | **Couche**       | **Filtre de données à appliquer**                                     |
| ----------------------------------------------------------------------------- | ---------------- | --------------------------------------------------------------------- |
| SisFrance - Intensité des épicentres en Outre-Mer                             | ms-SIS_OUTREMER  | longepc < 45.5 AND longepc > 44 AND latepc < -12.3 AND latepc > -13.3 |
| Sismicité - Intensité interpolée maximale possiblement ressentie par communes | ms-SIS_INTENSITE | "insee_com" LIKE '976%'                                               |

__Note__: au 14/09/2018, les dernières données de la couche ms-SIS_OUTREMER datent du 10/04/2018, et n'incluent donc pas les évênements récents à Mayotte (seuls 2 évênements sont répertoriés en 2004 et 2006) ; les données ms-SIS_INTENSITE semblent également obsolètes
__Mise à jour__: après avoir posé la question au BRGM, il est possible d'obtenir les données à des fin de recherche sous certaines conditions, mais les données, toujours en cours de validation, ne seront publiées que dans quelques mois

En revanche, elles sont désormais disponibles au format excel: [http://ressource.brgm.fr/data/data/372c5809-3d30-440c-b44a-1c89385f176a](http://ressource.brgm.fr/data/data/372c5809-3d30-440c-b44a-1c89385f176a)

## BRGM - Géologie

![Carte](./images/brgm-geologie-mayotte.png)

Service WFS: [http://geoservices.brgm.fr/geologie](http://geoservices.brgm.fr/geologie)

| **Donnée**                                    | **Couche**                 | **Filtre de données à appliquer** | **Commentaire**                              |
| --------------------------------------------- | -------------------------- | --------------------------------- | -------------------------------------------- |
| Points d'eau avec des mesures de niveau d'eau | ms-BSS_EAU_QT_POINT        | code_insee_actuel like '976%'     | afleurement d'eau, forage, puit, source, ... |
| Référentiel des points d'eau BSS EAU          | ms-BSS_EAU_POINT           | code_insee_actuel like '976%'     | afleurement d'eau, forage, puit, source, ... |
|                                               | ms-SCAN_H_GEOL50_PERIMETRE |                                   |                                              |
|                                               | ms-BSS_SEMIS_0             |                                   |                                              |
|                                               | ms-BSS_TOTAL_SANS_LABEL    |                                   |                                              |
|                                               | ms-BSS_TOTAL_AVEC_LABEL    |                                   |                                              |
|                                               | ms-BSS_EAU_DENSITE         |                                   |                                              |
|                                               | ms-BGM_STATION             |                                   |                                              |
|                                               | ms-BGM_TYPE                |                                   |                                              |
|                                               | ms-MINES_EMP               |                                   |                                              |

## BRGM - Granulats marins

![Carte](./images/brgm-granulats-marins-mayotte.png)

Service WFS: [http://geoservices.brgm.fr/odmgm](http://geoservices.brgm.fr/odmgm)

| **Donnée**                                          | **Couche**          | **Filtre de données à appliquer** | **Commentaire**                                                           |
| --------------------------------------------------- | ------------------- | --------------------------------- | ------------------------------------------------------------------------- |
| Contours des exploitations de matériaux en activité | ms-EXPLOIT_ACTIVE_L | departement=985 (échec!)          | flaggué avec ancien code departement=985, mais le filtre ne retourne rien |
| Contours des exploitations de matériaux fermées     | ms-EXPLOIT_FERMEE_L |                                   |                                                                           |
| Exploitations de matériaux en activité              | ms-EXPLOIT_ACTIV_P  |                                   |                                                                           |
| Exploitations de matériaux fermées                  | ms-EXPLOIT_FERMEE_P |                                   |                                                                           |
