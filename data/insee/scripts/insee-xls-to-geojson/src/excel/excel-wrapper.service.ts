import { Workbook, Worksheet } from 'exceljs';
import { from, Observable } from 'rxjs';

import { nodeLoggerService } from '../log/logger.service';

const Excel = require('exceljs');

export class ExcelWrapperService {
  
  readFile(filename: string): Observable<Workbook> {

    nodeLoggerService.debug('[ExcelWrapperService.readFile] filename: ', filename);

    const workbook = new Excel.Workbook();

    return from(workbook.xlsx.readFile(filename));

  }

  logWorksheetInfo(worksheet: Worksheet){
    nodeLoggerService.debug(`[ExcelWrapperService.logWorksheetInfo] open worksheet "${worksheet.name}" with ${worksheet.actualRowCount} rows and ${worksheet.actualColumnCount} columns`);
  }

}

export const nodeExcelWrapperService = new ExcelWrapperService();