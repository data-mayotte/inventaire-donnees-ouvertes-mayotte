import { nodeStringService } from '../../string/string-service';
import { Commune } from './communes-2012-excel-model';

export class Communes2012ExcelParserUtilService {

  parseCommuneLabel(label: string): Commune {

    const sourceId = nodeStringService.asNumber(nodeStringService.extractBetweenParenthesis(label));
    const name: string = nodeStringService.extractBefore(label, '\\(');

    return {
      name,
      sourceId,
    };
  }

}

export const nodeCommunes2012ExcelParserUtilService = new Communes2012ExcelParserUtilService();