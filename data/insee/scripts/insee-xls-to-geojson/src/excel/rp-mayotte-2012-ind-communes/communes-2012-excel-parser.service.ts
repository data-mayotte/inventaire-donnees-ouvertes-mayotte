import { Workbook } from 'exceljs';
import { Observable } from 'rxjs';
import { mapTo, tap } from 'rxjs/operators';

import { nodeLoggerService } from '../../log/logger.service';
import { nodeExcelWrapperService } from './../excel-wrapper.service';
import { Communes2012Excel } from './communes-2012-excel-model';
import { nodeCommunes2012ExcelParserInd1Service } from './communes-2012-excel-parser-ind1.service';
import { nodeCommunes2012ExcelParserSommaireService } from './communes-2012-excel-parser-sommaire.service';

export class Communes2012ExcelParserService {


  parse(filename: string): Observable<Communes2012Excel> {

    nodeLoggerService.debug('[Communes2012ExcelParserService.parse] filename:', filename);

    const model: Communes2012Excel = {} as Communes2012Excel;

    return nodeExcelWrapperService.readFile(filename).pipe(
      tap((workbook: Workbook) => nodeLoggerService.debug('[Communes2012ExcelParserService.parse] workbook last modified:', workbook.modified)),
      tap((workbook: Workbook) => model.sommaire = nodeCommunes2012ExcelParserSommaireService.parse(workbook)),
      tap((workbook: Workbook) => model.ind1 = nodeCommunes2012ExcelParserInd1Service.parse(workbook, model)),
      mapTo(model),
    );

  }
}

export const nodeCommunes2012ExcelParserService = new Communes2012ExcelParserService();