export interface Communes2012Excel {
  sommaire?: Communes2012ExcelSommaire;
  ind1?: Communes2012ExcelInd1;
}

export interface Commune {
  name: string;
  sourceId: number;
}

export interface TrancheAge {
  label: string;
  minAge: number;
  maxAge: number;
}

export interface Communes2012ExcelSommaire {
  tableauxIndividus: {
    key: string,
    label: string
  }[];
}

export interface Communes2012ExcelInd1 {
  key: string;
  description: string;
  ageParCommune: Communes2012ExcelInd1AgeParCommune[];
}

export interface Communes2012ExcelInd1AgeParCommune {
  commune: Commune;
  populationParTranche: {
    tranche: TrancheAge,
    population: number;
  }[];
}