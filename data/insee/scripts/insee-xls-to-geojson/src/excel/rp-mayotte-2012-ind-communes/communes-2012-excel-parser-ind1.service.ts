import { Workbook, Worksheet } from 'exceljs';

import { nodeExcelWrapperService } from './../excel-wrapper.service';
import { Communes2012Excel, Communes2012ExcelInd1 } from './communes-2012-excel-model';
import { nodeCommunes2012ExcelParserUtilService } from './communes-2013-excel-parser-util.service';

export class Communes2012ExcelParserInd1Service {

  public parse(workbook: Workbook, communes2012Excel: Communes2012Excel): Communes2012ExcelInd1 {

    const indice = communes2012Excel.sommaire.tableauxIndividus[0];
    const model: Communes2012ExcelInd1 = {
      key: indice.key,
      description: indice.label,
      ageParCommune: [],
    };

    const worksheet: Worksheet = workbook.getWorksheet(2);
    nodeExcelWrapperService.logWorksheetInfo(worksheet);

    for (let rowIndex = 5; rowIndex < 22; rowIndex++) {
      const communeCell = worksheet.getCell(rowIndex, 1);

      const communeLabel = communeCell.value as string;
      const commune = nodeCommunes2012ExcelParserUtilService.parseCommuneLabel(communeLabel);

      const populationParTranche = [];
      let minAge = 0;
      let maxAge = 4;
      for (let colIndex = 2; colIndex < 22; colIndex++ , minAge += 5, maxAge += 5) {
        const labelCell = worksheet.getCell(4, colIndex);
        const valueCell = worksheet.getCell(rowIndex, colIndex);
        populationParTranche.push({
          tranche: {
            label: labelCell.value as string,
            minAge: minAge,
            maxAge: maxAge !== 99 ? maxAge : undefined,
          },
          population: valueCell.value as number,
        });
      }
      model.ageParCommune.push({
        commune,
        populationParTranche,
      });
    }

    // nodeLoggerService.debug('[Communes2012ExcelParserInd1Service.parse] model:', JSON.stringify(model, undefined, 2));

    return model;
  }
}

export const nodeCommunes2012ExcelParserInd1Service = new Communes2012ExcelParserInd1Service();