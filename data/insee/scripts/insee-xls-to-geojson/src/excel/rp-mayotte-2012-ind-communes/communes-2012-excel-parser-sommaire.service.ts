import { Workbook, Worksheet } from 'exceljs';

import { nodeExcelWrapperService } from './../excel-wrapper.service';
import { Communes2012ExcelSommaire } from './communes-2012-excel-model';

export class Communes2012ExcelParserSommaireService {

  public parse(workbook: Workbook): Communes2012ExcelSommaire {

    const model: Communes2012ExcelSommaire = {
      tableauxIndividus: [],
    };

    const worksheet: Worksheet = workbook.getWorksheet(1);
    nodeExcelWrapperService.logWorksheetInfo(worksheet);

    for (let rowIndex = 4; rowIndex < 13; rowIndex++) {
      const keyCell = worksheet.getCell(rowIndex, 1);
      const labelCell = worksheet.getCell(rowIndex, 2);
      model.tableauxIndividus.push({
        key: keyCell.value as string,
        label: labelCell.value as string,
      });
    }

    // nodeLoggerService.debug('[Communes2012ExcelParserSommaireService.parse] model:', model);

    return model;
  }
}

export const nodeCommunes2012ExcelParserSommaireService = new Communes2012ExcelParserSommaireService();