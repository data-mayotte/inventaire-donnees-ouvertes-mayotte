export class StringService {

  extractBetweenParenthesis(s: string): string {

    if (!s) {
      return s;
    }

    const match = s.match(/\(([^)]+)\)/);
    const result = match != null && match.length > 1 ? match[1] : undefined;
    // nodeLoggerService.debug('[StringService.extractBetweenParenthesis] s:', s);
    // nodeLoggerService.debug('[StringService.extractBetweenParenthesis] match:', match);
    // nodeLoggerService.debug('[StringService.extractBetweenParenthesis] result:', result);
    return result;
  }
  extractBefore(s: string, beforeString: string): string {

    if (!s) {
      return s;
    }

    const match = s.match(new RegExp(`.+?(?= ${beforeString})`));
    const result = match != null && match.length > 0 ? match[0] : undefined;
    // nodeLoggerService.debug('[StringService.extractBefore] s:', s);
    // nodeLoggerService.debug('[StringService.extractBefore] match:', match);
    // nodeLoggerService.debug('[StringService.extractBefore] result:', result);
    return result;
  }

  asNumber(s: string): number {
    return s ? parseInt(s, 10) : undefined;
  }
}

export const nodeStringService = new StringService();