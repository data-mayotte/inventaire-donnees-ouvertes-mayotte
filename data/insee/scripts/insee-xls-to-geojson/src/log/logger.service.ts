import * as dateFns from 'date-fns';

const colors = require('colors/safe');
colors.setTheme({
  debug: 'white',
  info: 'cyan',
  warn: 'yellow',
  error: 'red'
});

export type LogLevel = 'debug' | 'info' | 'warn' | 'error' | 'disabled';

export class LoggerService {

  logLevel: LogLevel = 'debug';

  constructor() {
  }

  get debug() {
    return this.bindLog('debug');
  }

  get info() {
    return this.bindLog('info');
  }

  get warn() {
    return this.bindLog('warn');
  }

  get error() {
    return this.bindLog('error');
  }

  bindLog(logLevel: 'debug' | 'info' | 'warn' | 'error') {

    const logMethod = console[logLevel];

    return (message?: any, ...optionalParams: any[]): void => {

      message = `[${dateFns.format(new Date(), 'HH:mm:ss')}] ` + message;

      if (this.isLogLevelEnabled(logLevel)) {
        // tslint:disable-next-line:no-console
        let colorMessage;
        if (logLevel === 'error') {
          colorMessage = colors.error(message);
        } else if (logLevel === 'warn') {
          colorMessage = colors.warn(message);
        } else if (logLevel === 'info') {
          colorMessage = colors.info(message);
        } else {
          colorMessage = colors.debug(message);
        }
        logMethod.bind(console)(colorMessage, ...optionalParams);
      }

      // console.error('[LoggerService] bindLog', this.isSentryEnabled, logLevel);
    };
  }


  private isLogLevelEnabled(expectedLevel: 'debug' | 'info' | 'warn' | 'error'): boolean {

    switch (this.logLevel) {
      case 'disabled': {
        return false;
      }
      case 'error': {
        return expectedLevel === 'error';
      }
      case 'warn': {
        return expectedLevel === 'error' || expectedLevel === 'warn';
      }
      case 'info': {
        return expectedLevel === 'error' || expectedLevel === 'warn' || expectedLevel === 'info';
      }
      case 'debug': {
        return true;
      }
      default: {
        return false;
      }

    }
  }
}

export const nodeLoggerService = new LoggerService();