import fs from 'fs';
import osmtogeojson from 'osmtogeojson';
import { Observable, of } from 'rxjs';
import { map, mapTo, switchMap } from 'rxjs/operators';

import { nodeFileService } from '../file/file.service';
import { nodeHttpService } from '../network/http.service';

export class OsmOverpassService {

  query<T>(query: string, options: {
    cacheFile: string,
  }): Observable<T> {

    if (!options.cacheFile || !fs.existsSync(options.cacheFile)) {
      return this.overpassQuery<T>(query).pipe(
        switchMap(model => {
          if (options.cacheFile) {
            return nodeFileService.writeJson(options.cacheFile, model).pipe(
              mapTo(model),
            );
          } else {
            return of(model);
          }
        }),
      );
    } else {
      return nodeFileService.readJson<T>(options.cacheFile);
    }

  }

  private overpassQuery<T>(query: string): Observable<T> {

    const url = `https://overpass-api.de/api/interpreter?data=${encodeURI(query)}`;

    // nodeLoggerService.info('[OsmOverpassService.overpassQuery] url: ', url);

    return nodeHttpService.get(url).pipe(
      map(data => {
        const geojson = osmtogeojson(data, {
          verbose: true,
          flatProperties: false,
          uninterestingTags: undefined,
          polygonFeatures: undefined,
          deduplicator: undefined,
        });
        return geojson as unknown as T;
      }),

    );

  }
}

export const nodeOsmOverpassService = new OsmOverpassService();