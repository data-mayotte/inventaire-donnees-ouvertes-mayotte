import { FeatureCollection } from 'geojson';
import path from 'path';
import { Observable } from 'rxjs';

import { nodeOsmOverpassService } from './osm-overpass.service';

export class OsmService {

  getCommunesMayotte(options: {
    workingDir: string,
  }): Observable<FeatureCollection> {

    const q = '[out:json];\
    rel\
    [admin_level=8]\
    [type=boundary]\
    [boundary=administrative]\
    (-13.3,44.7,-12.3,45.5);\
    out geom;';

    return nodeOsmOverpassService.query<FeatureCollection>(q, {
      cacheFile: path.join(options.workingDir, 'osm', 'osm-mayotte-communes.geojson'),
    });

  }
}

export const nodeOsmService = new OsmService();