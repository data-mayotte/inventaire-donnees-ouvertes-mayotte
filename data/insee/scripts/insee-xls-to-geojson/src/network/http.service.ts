import fs from 'fs';
import http from 'http';
import https from 'https';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { nodeFileService } from '../file/file.service';
import { nodeLoggerService } from '../log/logger.service';

export class HttpService {

  get<T>(url: string, format: 'json' | 'text' = 'json'): Observable<T> {

    const httpOrHttps: any = this.getHttpOrHttps(url);

    return Observable.create((observer) => {

      httpOrHttps.get(url, (res) => {
        let body = '';

        res.on('data', (chunk) => {
          body += chunk;
        });

        res.on('end', () => {
          if (format === 'json') {
            observer.next(JSON.parse(body));
          } else {
            observer.next(body);
          }

          observer.complete();
        });
      }).on('error', (e) => {
        nodeLoggerService.error('[HttpService.get] ERROR: ', e);
        observer.error(e);
      });
    });
  }

  downloadJson<T>(options: {
    url: string,
    destFile: string,
    override: boolean,
  }): Observable<T> {

    if (options.override || !fs.existsSync(options.destFile)) {
      return this.downloadFile(options).pipe(
        switchMap(() => nodeFileService.readJson(options.destFile)),
      );
    } else {
      return nodeFileService.readJson(options.destFile);
    }
  }

  private getHttpOrHttps(url: string): any {
    return url.indexOf('https') === 0 ? https : http;
  }

  private downloadFile<T>(options: {
    url: string,
    destFile: string,
  }): Observable<T> {

    nodeLoggerService.info('[HttpService.downloadFile] options: ', options);

    const httpOrHttps: any = this.getHttpOrHttps(options.url);

    const file = fs.createWriteStream(options.destFile, 'utf8');
    return Observable.create(function (observer) {

      httpOrHttps.get(options.url, (res) => {
        res.pipe(file);

        let body = '';

        res.on('data', (chunk) => {
          body += chunk;
        });

        res.on('end', () => {
          file.end();
          observer.next();
          observer.complete();
        });
      }).on('error', (e) => {
        nodeLoggerService.error('[HttpService.download] ERROR: ', e);
        observer.error(e);
      });
    });

  }
}


export const nodeHttpService = new HttpService();