import { FeatureCollection } from 'geojson';

import {
  Communes2012Excel,
  Communes2012ExcelInd1AgeParCommune,
} from '../../excel/rp-mayotte-2012-ind-communes/communes-2012-excel-model';
import { nodeLoggerService } from '../../log/logger.service';
import {
  Communes2012Ind1GeojsonFlatFeatureCollection,
  Communes2012Ind1GeojsonFlatFeatureProperties,
} from './communes-2012-ind1-geojson-flat.model';
import { nodeCommunes2012Ind1GeojsonUtilService } from './communes-2013-ind1-geojson-util.service';

export class Communes2012Ind1GeojsonFlatBuilderService {

  public buildFeatureCollection(options: {
    communesFeatures: FeatureCollection;
    communes2012ExcelModel: Communes2012Excel;
  }): Communes2012Ind1GeojsonFlatFeatureCollection {

    const filteredFeatures = options.communesFeatures.features.filter(f => f.properties && f.properties.tags && f.properties.tags.name);

    const features = filteredFeatures.map(communeFeature => {
      const communeName = communeFeature.properties.tags.name;

      const inseeData: Communes2012ExcelInd1AgeParCommune = nodeCommunes2012Ind1GeojsonUtilService.getAgeByCommune(options.communes2012ExcelModel, communeName);

      const properties: Communes2012Ind1GeojsonFlatFeatureProperties = {
        ...communeFeature.properties,
        name: communeFeature.properties.tags.name,
        totalPopulation: 0,
      };

      // add flat inseeData,
      inseeData.populationParTranche.forEach(pop => {
        const propertyName = pop.tranche.maxAge ? `${pop.tranche.minAge}-${pop.tranche.maxAge}` : `${pop.tranche.minAge}+`;
        properties[propertyName] = pop.population;
        properties.totalPopulation += pop.population;
      });

      this.buildAgeGroupsBy20(properties, inseeData);

      return {
        ...communeFeature,
        properties
      };
    });

    return {
      type: options.communesFeatures.type,
      features
    };
  }

  private buildAgeGroupsBy20(properties: Communes2012Ind1GeojsonFlatFeatureProperties, inseeData: Communes2012ExcelInd1AgeParCommune) {
    properties['0-19'] = 0;
    properties['20-39'] = 0;
    properties['40-59'] = 0;
    properties['60-79'] = 0;
    properties['80+'] = 0;
    // add groups of age by 20 years
    inseeData.populationParTranche.forEach(pop => {
      switch (pop.tranche.minAge) {
        case (0):
        case (5):
        case (10):
        case (15):
          properties['0-19'] += pop.population;
          break;
        case (20):
        case (25):
        case (30):
        case (35):
          properties['20-39'] += pop.population;
          break;
        case (40):
        case (45):
        case (50):
        case (55):
          properties['40-59'] += pop.population;
          break;
        case (60):
        case (65):
        case (70):
        case (75):
          properties['60-79'] += pop.population;
          break;
        case (80):
        case (85):
        case (90):
        case (95):
          properties['80+'] += pop.population;
          break;
        default:
          nodeLoggerService.error('[Communes2012Ind1GeojsonFlatBuilderService] unexpected minAge:', pop.tranche.minAge);
          break;
      }
    });
  }
}

export const nodeCommunes2012Ind1GeojsonFlatBuilderService = new Communes2012Ind1GeojsonFlatBuilderService();