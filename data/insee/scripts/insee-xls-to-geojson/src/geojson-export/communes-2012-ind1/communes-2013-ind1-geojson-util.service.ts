import {
  Communes2012Excel,
  Communes2012ExcelInd1AgeParCommune,
} from '../../excel/rp-mayotte-2012-ind-communes/communes-2012-excel-model';
import { nodeLoggerService } from '../../log/logger.service';

export class Communes2012Ind1GeojsonUtilService {

  public getAgeByCommune(communes2012ExcelModel: Communes2012Excel, communeName: string): Communes2012ExcelInd1AgeParCommune {

    const ind1 = communes2012ExcelModel.ind1.ageParCommune.find(apc => apc.commune.name === communeName || apc.commune.name.replace(/é/g, 'e').toLowerCase() === communeName.replace(/é/g, 'e').toLowerCase());

    if (!ind1) {
      nodeLoggerService.error(`[Communes2012Ind1GeojsonStructuredBuilderService.buildFeatureCollection] ind1 data not fount for commune ${communeName}`);
    }
    return ind1;
  }
}

export const nodeCommunes2012Ind1GeojsonUtilService = new Communes2012Ind1GeojsonUtilService();