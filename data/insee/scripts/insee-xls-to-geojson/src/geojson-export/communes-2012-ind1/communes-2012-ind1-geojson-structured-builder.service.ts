import { FeatureCollection } from 'geojson';

import {
  Communes2012Excel,
  Communes2012ExcelInd1AgeParCommune,
} from '../../excel/rp-mayotte-2012-ind-communes/communes-2012-excel-model';
import {
  Communes2012Ind1GeojsonStructuredFeatureCollection,
  Communes2012Ind1GeojsonStructuredFeatureProperties,
} from './communes-2012-ind1-geojson-structured.model';
import { nodeCommunes2012Ind1GeojsonUtilService } from './communes-2013-ind1-geojson-util.service';

export class Communes2012Ind1GeojsonStructuredBuilderService {

  public buildFeatureCollection(options: {
    communesFeatures: FeatureCollection;
    communes2012ExcelModel: Communes2012Excel;
  }): Communes2012Ind1GeojsonStructuredFeatureCollection {

    const filteredFeatures = options.communesFeatures.features.filter(f => f.properties && f.properties.tags && f.properties.tags.name);

    const features = filteredFeatures.map(communeFeature => {
      const communeName = communeFeature.properties.tags.name;
      
      const inseeData: Communes2012ExcelInd1AgeParCommune = nodeCommunes2012Ind1GeojsonUtilService.getAgeByCommune(options.communes2012ExcelModel, communeName);

      const properties: Communes2012Ind1GeojsonStructuredFeatureProperties = {
        ...communeFeature.properties,
        osm: communeFeature.properties.tags,
        insee: inseeData,
      };
      return {
        ...communeFeature,
        properties
      };
    });

    return {
      type: options.communesFeatures.type,
      features
    };
  }
}

export const nodeCommunes2012Ind1GeojsonStructuredBuilderService = new Communes2012Ind1GeojsonStructuredBuilderService();