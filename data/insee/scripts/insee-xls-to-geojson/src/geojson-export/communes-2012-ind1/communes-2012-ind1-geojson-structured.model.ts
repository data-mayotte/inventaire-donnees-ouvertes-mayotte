import { FeatureCollection, GeoJsonProperties, Geometry, GeometryObject } from 'geojson';

import { Communes2012ExcelInd1AgeParCommune } from '../../excel/rp-mayotte-2012-ind-communes/communes-2012-excel-model';

export interface Communes2012Ind1GeojsonStructuredFeatureCollection<G extends GeometryObject | null = Geometry> extends FeatureCollection<G, Communes2012Ind1GeojsonStructuredFeatureProperties> {
}

export interface Communes2012Ind1GeojsonStructuredFeatureProperties extends GeoJsonProperties {
  osm: GeoJsonProperties; // osm tags
  insee: Communes2012ExcelInd1AgeParCommune;
}