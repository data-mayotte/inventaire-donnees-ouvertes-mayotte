import { FeatureCollection, GeoJsonProperties, Geometry, GeometryObject } from 'geojson';

export interface Communes2012Ind1GeojsonFlatFeatureCollection<G extends GeometryObject | null = Geometry> extends FeatureCollection<G, Communes2012Ind1GeojsonFlatFeatureProperties> {
}

export interface Communes2012Ind1GeojsonFlatFeatureProperties extends GeoJsonProperties {
  name: string; // osm tags
  totalPopulation: number; // osm tags
}