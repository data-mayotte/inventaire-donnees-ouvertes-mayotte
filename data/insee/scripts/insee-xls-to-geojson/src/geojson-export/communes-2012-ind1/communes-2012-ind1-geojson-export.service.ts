import fs from 'fs';
import { FeatureCollection } from 'geojson';
import path from 'path';
import { Observable } from 'rxjs';
import { map, mapTo, switchMap } from 'rxjs/operators';

import { Communes2012Excel } from '../../excel/rp-mayotte-2012-ind-communes/communes-2012-excel-model';
import { nodeFileService } from '../../file/file.service';
import { nodeLoggerService } from '../../log/logger.service';
import { nodeCommunes2012Ind1GeojsonFlatBuilderService } from './communes-2012-ind1-geojson-flat-builder.service';
import {
  nodeCommunes2012Ind1GeojsonStructuredBuilderService,
} from './communes-2012-ind1-geojson-structured-builder.service';

const GROUP_NAME = 'rp-mayotte-2012-ind-communes';
const DATA_NAME = 'ind1-population-par-commune-de-residence-et-age-quinquennal';

export class Communes2012Ind1GeojsonExportService {

  exportToGeojson(options: {
    communesFeatures: FeatureCollection,
    communes2012ExcelModel: Communes2012Excel,
    staticDir: string;
  }): Observable<any> {

    nodeLoggerService.debug('[Communes2012GeojsonExportService.export]');

    const exportDir = this.buildExportDir(options.staticDir);

    return this.exportStructured({
      ...options,
      exportDir
    }).pipe(
      switchMap(structured => this.exportFlat({
        ...options,
        exportDir,
      }).pipe(
        map(flat => ({
          flat,
          structured,
        })),
      )),
    );

  }

  private exportStructured(options: {
    exportDir: string,
    communesFeatures: FeatureCollection;
    communes2012ExcelModel: Communes2012Excel,
  }): Observable<any> {

    nodeLoggerService.debug('[Communes2012GeojsonExportService.exportStructured]');

    const structuredFeatureCollection = nodeCommunes2012Ind1GeojsonStructuredBuilderService.buildFeatureCollection(options);
    const destFileStructured = path.join(options.exportDir, `${DATA_NAME}.structured.geojson`);
    return nodeFileService.writeJson(destFileStructured, structuredFeatureCollection).pipe(
      mapTo({
        data: structuredFeatureCollection,
        file: destFileStructured,
      }),
    );
  }

  private exportFlat(options: {
    exportDir: string,
    communesFeatures: FeatureCollection;
    communes2012ExcelModel: Communes2012Excel,
  }): Observable<any> {

    nodeLoggerService.debug('[Communes2012GeojsonExportService.exportFlat]');

    const destFileFlat = path.join(options.exportDir, `${DATA_NAME}.flat.geojson`);

    const communesFeaturesWithDataFlat = nodeCommunes2012Ind1GeojsonFlatBuilderService.buildFeatureCollection(options);
    return nodeFileService.writeJson(destFileFlat, communesFeaturesWithDataFlat).pipe(
      mapTo({
        data: communesFeaturesWithDataFlat,
        file: destFileFlat,
      }),
    );
  }

  private buildExportDir(staticDir: string) {
    let dir = staticDir;
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    dir = path.join(dir, GROUP_NAME);
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    dir = path.join(dir, DATA_NAME);
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    return dir;
  }

}

export const nodeCommunes2012Ind1GeojsonExportService = new Communes2012Ind1GeojsonExportService();