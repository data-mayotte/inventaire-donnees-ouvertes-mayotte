import { FeatureCollection } from 'geojson';
import path from 'path';
import { switchMap } from 'rxjs/operators';

import { Communes2012Excel } from './excel/rp-mayotte-2012-ind-communes/communes-2012-excel-model';
import { nodeCommunes2012ExcelParserService } from './excel/rp-mayotte-2012-ind-communes/communes-2012-excel-parser.service';
import { nodeFileService } from './file/file.service';
import {
  nodeCommunes2012Ind1GeojsonExportService,
} from './geojson-export/communes-2012-ind1/communes-2012-ind1-geojson-export.service';
import { nodeOsmService } from './osm/osm.service';

const staticDir = path.join(__dirname, '../../../static');
const sourceFile = path.join(staticDir, 'rp-mayotte-2012-ind-communes', 'rp-mayotte-2012-ind-communes.xlsx');


nodeOsmService.getCommunesMayotte({
  workingDir: staticDir
}).pipe(
  switchMap((communesFeatures: FeatureCollection) => {
    return nodeCommunes2012ExcelParserService.parse(sourceFile).pipe(
      switchMap((communes2012ExcelModel: Communes2012Excel) => {
        const destFile = path.join(staticDir, 'rp-mayotte-2012-ind-communes', 'rp-mayotte-2012-ind-communes.json');
        return nodeFileService.writeJson(destFile, communes2012ExcelModel);
      }),
      switchMap(communes2012ExcelModel => nodeCommunes2012Ind1GeojsonExportService.exportToGeojson({
        communesFeatures,
        communes2012ExcelModel,
        staticDir
      }))
    );
  }),
).subscribe();
