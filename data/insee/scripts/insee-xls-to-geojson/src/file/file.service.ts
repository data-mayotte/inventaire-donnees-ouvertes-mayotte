import fs from 'fs';
import { Observable, of } from 'rxjs';

import { nodeLoggerService } from '../log/logger.service';

export class FileService {

  readJson<T>(path: string): Observable<T> {
    // TODO transform to async
    const content = fs.readFileSync(path, 'utf8');
    return of(JSON.parse(content));
  }

  writeJson<T>(destFile: string, model: T): Observable<T> {
    // TODO transform to async
    fs.writeFileSync(destFile, JSON.stringify(model, undefined, 2), 'utf8');
    nodeLoggerService.info(`Write file ${destFile}`);
    return of(model);
  }
}


export const nodeFileService = new FileService();