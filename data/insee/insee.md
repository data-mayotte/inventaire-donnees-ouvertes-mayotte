# INSEE (Institut National de la Statistique et des Etudes Economiques - données sur Mayotte

[https://www.insee.fr](https://www.insee.fr)

API: [https://api.insee.fr/catalogue/](https://api.insee.fr/catalogue/)

## Recensement de la population

[https://www.insee.fr/fr/information/2411261](https://www.insee.fr/fr/information/2411261)

Données disponibles:

- Principaux résultats pour le recensement de 2012
  - [Population sur les communes et villages](https://www.insee.fr/fr/statistiques/2409395?sommaire=2409812)
  - [Tableaux de logements et résidences principales par communes et villages](https://www.insee.fr/fr/statistiques/2409389?sommaire=2409812)
- Résultats détaillés pour le recensement de 2012
  - Tableaux détaillés Mayotte : [population active](https://www.insee.fr/fr/statistiques/2408585?sommaire=2409812)
  - Tableaux détaillés Mayotte : [emplois au lieu de travail](https://www.insee.fr/fr/statistiques/2409953?sommaire=2409812)
  - Tableaux détaillés Mayotte : [familles](https://www.insee.fr/fr/statistiques/2409956?sommaire=2409812)
  - Tableaux détaillés Mayotte : [formation](https://www.insee.fr/fr/statistiques/2409959?sommaire=2409812)
  - Tableaux détaillés Mayotte : [logements](https://www.insee.fr/fr/statistiques/2409962?sommaire=2409812)
  - Tableaux détaillés Mayotte : [ménages](https://www.insee.fr/fr/statistiques/2409965?sommaire=2409812)
  - Tableaux détaillés Mayotte : [nationalité](https://www.insee.fr/fr/statistiques/2409968?sommaire=2409812)
  - Tableaux détaillés Mayotte : [population](https://www.insee.fr/fr/statistiques/2409971?sommaire=2409812)

- [Résultats détaillés pour le recensement de 2007](https://www.insee.fr/fr/statistiques/2569525)

![Carte](./images/repartition-population-tranches-age-mayotte.png)
