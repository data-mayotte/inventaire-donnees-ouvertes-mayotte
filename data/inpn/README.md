# INPN (Inventaire National du Patrimoine Naturel) - données sur Mayotte

[https://inpn.mnhn.fr](https://inpn.mnhn.fr)

[https://inpn.mnhn.fr/collTerr/outreMer/choix/976](https://inpn.mnhn.fr/collTerr/outreMer/choix/976)

## Liste des espaces protégés et gérés

[https://inpn.mnhn.fr/collTerr/outreMer/976/MYT/tab/espaces](https://inpn.mnhn.fr/collTerr/outreMer/976/MYT/tab/espaces)
