# Ifrecor - données sur Mayotte

Initiative française pour les récifs coralliens.

[http://www.ifrecor.com/comites-locaux-r4-mayotte.html](http://www.ifrecor.com/comites-locaux-r4-mayotte.html)

## Les mangroves

[[Etude] Les mangroves de l'outre-mer français - Ecosystèmes associés aux récifs coralliens](http://ifrecor-doc.fr/items/show/1481)

## Les herbiers

[[Etude] Les herbiers de phanérogames marines de l'outre-mer français - Écosystèmes associés aux récifs coralliens](http://ifrecor-doc.fr/items/show/1479)
