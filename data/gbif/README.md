
# GBIF (Global Biodiversity Information Facility) - données sur Mayotte

Système mondial d'informations sur la biodiversité

> Le GBIF pour Global Biodiversity Information Facility (en français « système mondial d'informations sur la biodiversité ») est un projet scientifique international, fondé en forme de consortium par l'OCDE en 2001, et qui a pour but de mettre à disposition toute l'information connue sur la biodiversité (données d'observations ou de collections sur les animaux, plantes, champignons, bactéries et archaea).

[Téléchargement zone géographique Mayotte](https://www.gbif.org/occurrence/map?has_geospatial_issue=false&geometry=POLYGON((44.64844%20-13.43491,45.70587%20-13.43216,45.66193%20-12.36649,44.73083%20-12.22366,44.78302%20-12.71187,44.64844%20-13.43491))
)

Liens:

- [https://fr.wikipedia.org/wiki/Global_Biodiversity_Information_Facility](https://fr.wikipedia.org/wiki/Global_Biodiversity_Information_Facility)
- [https://www.gbif.org](https://www.gbif.org)