
# Geo Littoral - données sur Mayotte

[http://www.geolittoral.developpement-durable.gouv.fr](http://www.geolittoral.developpement-durable.gouv.fr)

[Services WMS & WFS](http://www.geolittoral.developpement-durable.gouv.fr/services-web-d-interoperabilite-a803.html)

## Geo Littoral - Traits de côte

Service WFS: [http://geolittoral.din.developpement-durable.gouv.fr/wxs](http://geolittoral.din.developpement-durable.gouv.fr/wxs)

|           **Donnée**            |             **Couche**              |
| ------------------------------- | ----------------------------------- |
| Traits de côte naturels récents | ms:n_traits_cote_naturels_recents_l |
