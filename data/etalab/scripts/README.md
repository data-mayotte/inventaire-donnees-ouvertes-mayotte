# Téléchargement des données de l'etalab

## Description

Les données du Etalab ne sont pas stockées dans le projet afin de ne pas le surcharger.

Ce script permet de les télécharger.

Ce script fonctionne sous linux et mac.

## Utilisation

Pour mettre à jour toutes les données:

```bash
./download-etalab-data.sh
```

Les fichiers sont générés dans:

- /data/etalab/static

## Licence

![Public Domain](https://i.creativecommons.org/p/zero/1.0/88x31.png "Public Domain").

Ces scripts sont libres de droits (licence [CC0 1.0 universel (CC0 1.0)](https://creativecommons.org/publicdomain/zero/1.0/deed.fr)).