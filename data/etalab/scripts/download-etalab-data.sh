#!/bin/bash

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
USGS_DIR="$( cd "$( dirname "${BASE_DIR}" )" && pwd )"

STATIC_DATA_DIR=$USGS_DIR/static
mkdir -p $STATIC_DATA_DIR


# shapefile
curl -o "$STATIC_DATA_DIR/OSM_BAN_odbl_976-shp.zip" https://bano.openstreetmap.fr/BAN_odbl/BAN_odbl_976-shp.zip
curl -o "$STATIC_DATA_DIR/DATAGOUV_BAN_odbl_976-shp.zip" https://adresse.data.gouv.fr/data/BAN_licence_gratuite_repartage_976.zip



ETALAB_CADASTRE_RELEASE="latest"
ETALAB_CADASTRE_BASE_URL="https://cadastre.data.gouv.fr/data/etalab-cadastre/${ETALAB_CADASTRE_RELEASE}/geojson/departements/976"

# cadastre
# ETALAB_CADASTRE_BASE_URL="https://cadastre.data.gouv.fr/data/etalab-cadastre/latest/geojson/departements/976/"
curl -o "$STATIC_DATA_DIR/etalab-cadastre-976-batiments.json.gz" ${ETALAB_CADASTRE_BASE_URL}/cadastre-976-batiments.json.gz
curl -o "$STATIC_DATA_DIR/etalab-cadastre-976-communes.json.gz" ${ETALAB_CADASTRE_BASE_URL}/cadastre-976-communes.json.gz
curl -o "$STATIC_DATA_DIR/etalab-cadastre-976-feuilles.json.gz" ${ETALAB_CADASTRE_BASE_URL}/cadastre-976-feuilles.json.gz
curl -o "$STATIC_DATA_DIR/etalab-cadastre-976-lieux_dits.json.gz" ${ETALAB_CADASTRE_BASE_URL}/cadastre-976-lieux_dits.json.gz
curl -o "$STATIC_DATA_DIR/etalab-cadastre-976-parcelles.json.gz" ${ETALAB_CADASTRE_BASE_URL}/cadastre-976-parcelles.json.gz
curl -o "$STATIC_DATA_DIR/etalab-cadastre-976-prefixes_sections.json.gz" ${ETALAB_CADASTRE_BASE_URL}/cadastre-976-prefixes_sections.json.gz
curl -o "$STATIC_DATA_DIR/etalab-cadastre-976-subdivisions_fiscales.json.gz" ${ETALAB_CADASTRE_BASE_URL}/cadastre-976-subdivisions_fiscales.json.gz
