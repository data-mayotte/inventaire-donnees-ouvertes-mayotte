
# Etalab - données sur Mayotte

## Cadastre Etalab

[Cadastre](https://cadastre.data.gouv.fr/datasets/cadastre-etalab)

[Téléchargement des données](https://cadastre.data.gouv.fr/data/etalab-cadastre/latest/geojson/departements/976/)

[Licence Ouverte](https://www.data.gouv.fr/fr/licences)

# Base Adresse Nationale (BAN)

> La Base Adresse Nationale est une base de données qui a pour but de référencer l'intégralité des adresses du territoire français.
> Elle est constituée par la collaboration entre Etalab, La Poste, l'IGN, la DGFiP et OpenStreetMap France.

[Base Adresse Nationale](https://www.data.gouv.fr/fr/datasets/base-adresse-nationale/)
[Téléchargement des données](https://bano.openstreetmap.fr/BAN_odbl):
[Service de géocodage gratuit](https://adresse.data.gouv.fr/api):

- [json](https://bano.openstreetmap.fr/BAN_odbl/BAN_odbl_976-json.bz2)
- [csv](https://bano.openstreetmap.fr/BAN_odbl/BAN_odbl_976-csv.bz2)
- [shapefile](https://bano.openstreetmap.fr/BAN_odbl/BAN_odbl_976-shp.zip)

[Licence ODbL](https://www.openstreetmap.org/copyright)