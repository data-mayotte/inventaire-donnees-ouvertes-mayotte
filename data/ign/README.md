# IGN (Institut National de l'Information Géographique et Forestière) - données sur Mayotte

[http://professionnels.ign.fr/donnees](http://professionnels.ign.fr/donnees)

## Contours Iris® - fond numérisé des îlots Iris

[http://professionnels.ign.fr/contoursiris](http://professionnels.ign.fr/contoursiris)

> Coédition INSEE et IGN, Contours...Iris® est un fond numérisé des îlots Iris définis par l'INSEE pour les besoins des recensements sur l'ensemble des communes de plus de 10 000 habitants et la plupart des communes de 5 000 à 10 000 habitants.

Version disponible: données France entière découpées par territoire édition 2017 Version 2.1

[Licence Ouverte version 1.0](https://www.data.gouv.fr/fr/licences)

## BD TOPO® - Hydrographie

[http://professionnels.ign.fr/bdtopo-hydrographie](http://professionnels.ign.fr/bdtopo-hydrographie)

Version disponible: thème Hydrographie de la BD TOPO® édition avril 2017 sur Mayotte

[Licence Ouverte version 1.0](https://www.data.gouv.fr/fr/licences)

## FranceRaster®

[http://professionnels.ign.fr/franceraster](http://professionnels.ign.fr/franceraster)

> pyramide d’images cartographiques numériques

Version disponible: FranceRaster® Ed 2017 du 1:100.000 au 1:250.000 sur Mayotte

[Licence Ouverte version 2.0](https://www.data.gouv.fr/fr/licences)

## BD ORTHO® 5 m

[http://professionnels.ign.fr/bdortho-5m](http://professionnels.ign.fr/bdortho-5m)

> orthophotographie départementale de l'IGN à une résolution de 5 mètres

Version disponible: BD ORTHO® 5 m de Mayotte (976)

[Licence Ouverte version 2.0](https://www.data.gouv.fr/fr/licences)

## Admin Express - découpage administratif du territoire

[http://professionnels.ign.fr/adminexpress](http://professionnels.ign.fr/adminexpress)

Version disponible: ADMIN EXPRESS édition 2018 sur l'ensemble des Départements français

[Licence Ouverte version 2.0](https://www.data.gouv.fr/fr/licences)

## GEOFLA® - découpage administratif national

[http://professionnels.ign.fr/geofla](http://professionnels.ign.fr/geofla)

Version disponible: ADMIN EXPRESS édition 2018 sur l'ensemble des Départements français

[Licence Ouverte version 1.0](https://www.data.gouv.fr/fr/licences)

## RGP - registre parcellaire graphique

[http://professionnels.ign.fr/rpg](http://professionnels.ign.fr/rpg)
> Le registre parcellaire graphique est une base de données géographiques servant de référence à l'instruction des aides de la politique agricole commune (PAC).
> La version anonymisée diffusée ici dans le cadre du service public de mise à disposition des données de référence contient les données graphiques des îlots (unité foncière de base de la déclaration des agriculteurs) munis de leur culture principale. Ces données sont produites par l'agence de services et de paiement (ASP) depuis 2007.

Version disponible: RPG de la région Mayotte édition 2016

Données vecteurs:

- ILOTS_ANONYMES
- PARCELLES_GRAPHIQUES

[Licence Ouverte version 2.0](https://www.data.gouv.fr/fr/licences)

## Liens utiles

- [Couverture des données IGN](http://professionnels.ign.fr/couverture-des-donnees)
- [Données IGN en open-data](http://www.ign.fr/institut/activites/lign-lopen-data)
- [Gratuité des données](http://professionnels.ign.fr/gratuite-des-donnees)
- [Documentation technique de l'API Géoportail](https://depot.ign.fr/geoportail/api/develop/tech-docs-js/fr/webmaster/flex/add_layers_as.html)
- [Ressources WFS](https://geoservices.ign.fr/documentation/donnees-ressources-wfs.html)
- [Ressources WMS](https://geoservices.ign.fr/documentation/donnees-ressources-wms-geoportail.html)
- [Ressources WMTS](https://geoservices.ign.fr/documentation/donnees-ressources-wmts.html)
- [Utilisation service OGC WFS](https://geoservices.ign.fr/documentation/geoservices/wfs.html)