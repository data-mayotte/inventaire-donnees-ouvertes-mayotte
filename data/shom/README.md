# SHOM - données sur Mayotte

Information géographique maritime et littorale de référence

[http://www.shom.fr/les-services-en-ligne/portail-datashomfr/](http://www.shom.fr/les-services-en-ligne/portail-datashomfr/)

## Litto3D

> modèle numérique altimétrique de référence, continu terre-mer et précis, sur la frange du littoral métropolitain et ultramarin

[https://www.data.gouv.fr/fr/datasets/mnt-littoral-litto3d-r-mayotte-2012/](https://www.data.gouv.fr/fr/datasets/mnt-littoral-litto3d-r-mayotte-2012/)
[http://diffusion.shom.fr/pro/amenagement/altimetrie-littorale/litto3d-mayot2012.html](http://diffusion.shom.fr/pro/amenagement/altimetrie-littorale/litto3d-mayot2012.html)

Version disponible: MNT littoral Litto3D® - Mayotte 2012 

Le téléchargement est découpé par zones:

![Litto3D - zones](./images/shom-litto-3d-zones.png)

[Licence ouverte](https://www.etalab.gouv.fr/wp-content/uploads/2014/05/Licence_Ouverte.pdf)

![Litto3D - apperçu](http://diffusion.shom.fr/media/catalog/product/cache/6/thumbnail/800x800/9df78eab33525d08d6e5fb8d27136e95/m/a/mayotte-vign.png)

## DELMAR - Limites maritimes

|                                **Liens**                                 | **Licence** |
| ------------------------------------------------------------------------ | ----------- |
| [https://limitesmaritimes.gouv.fr](https://limitesmaritimes.gouv.fr)     |             |
| [Services WMS & WFS](https://limitesmaritimes.gouv.fr/carte-interactive) |             |

![Carte](./images/screenshot/delmar.png "DELMAR - Mayotte")

Filtres utiles:

- "territory" = 'La Réunion - Mayotte - îles Eparses'

Service WFS: [http://services.data.shom.fr/DELMAR/wfs](http://services.data.shom.fr/DELMAR/wfs)

|                            **Donnée**                            |                          **Couche**                          |
| ---------------------------------------------------------------- | ------------------------------------------------------------ |
| Limites maritimes: au_baseline                                   | DELMAR_BDD_WFS:au_baseline                                   |
| Limites maritimes: au_maritimeboundary_agreedmaritimeboundary    | DELMAR_BDD_WFS:au_maritimeboundary_agreedmaritimeboundary    |
| Limites maritimes: au_maritimeboundary_contiguouszone            | DELMAR_BDD_WFS:au_maritimeboundary_contiguouszone            |
| Limites maritimes: au_maritimeboundary_economicexclusivezone     | DELMAR_BDD_WFS:au_maritimeboundary_economicexclusivezone     |
| Limites maritimes: au_maritimeboundary_nonagreedmaritimeboundary | DELMAR_BDD_WFS:au_maritimeboundary_nonagreedmaritimeboundary |
| Limites maritimes: au_maritimeboundary_territorialsea            | DELMAR_BDD_WFS:au_maritimeboundary_territorialsea            |
