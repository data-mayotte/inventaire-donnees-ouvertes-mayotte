
# Sextant Ifremer - données sur Mayotte

[https://sextant.ifremer.fr](https://sextant.ifremer.fr)

[Services WMS, WFS et CSW](https://sextant.ifremer.fr/en/nos-services)

## Sextant Ifremer - Biologie

Marine Habitats, fisheries, marine mammals, etc.

Service WFS: [http://www.ifremer.fr/services/wfs/biologie](http://www.ifremer.fr/services/wfs/biologie)

|                            **Donnée**                            |                          **Couche**                          |
| ---------------------------------------------------------------- | ------------------------------------------------------------ |
| TODO            |           |

## Sextant Ifremer - Surveillance littorale

Active coastal monitoring networks, historical data, etc.

Service WFS: [http://www.ifremer.fr/services/wfs/surveillance_littorale](http://www.ifremer.fr/services/wfs/surveillance_littorale)

|                            **Donnée**                            |                          **Couche**                          |
| ---------------------------------------------------------------- | ------------------------------------------------------------ |
| TODO            |           |

## Sextant Ifremer - Granulats marins

Mineral resources

Service WFS: [http://www.ifremer.fr/services/wfs/granulats_marins](http://www.ifremer.fr/services/wfs/granulats_marins)

|                            **Donnée**                            |                          **Couche**                          |
| ---------------------------------------------------------------- | ------------------------------------------------------------ |
| TODO            |           |
