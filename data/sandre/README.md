
# SANDRE - données sur Mayotte

Référentiel des données sur l'eau

|                                                **Liens**                                                | **Licence** |
| ------------------------------------------------------------------------------------------------------- | ----------- |
| [http://sandre.eaufrance.fr](http://sandre.eaufrance.fr)                                                |             |
| [Données pour Mayotte](http://sandre.eaufrance.fr/atlas/srv/fre/catalog.search#/search?keyword=MAYOTTE) |             |

## SANDRE - Masses d'eau

Service WFS: [http://services.sandre.eaufrance.fr/geo/MasseDEau_VRAP2016?VERSION=1.1.0](http://services.sandre.eaufrance.fr/geo/MasseDEau_VRAP2016?VERSION=1.1.0)

|                                                                          **Donnée**                                                                           |       **Couche**       |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------- |
| Entités hydrogéologiques de niveau 3 - Mayotte                                                                                                                | sa:EntiteHydroGeol_MYT |
| Entités hydrogéologiques: couches WFS: [http://services.sandre.eaufrance.fr/geo/saq?VERSION=1.1.0](http://services.sandre.eaufrance.fr/geo/saq?VERSION=1.1.0) |                        |

|                               **Donnée**                               |       **Couche**       |
| ---------------------------------------------------------------------- | ---------------------- |
| Polygones élémentaires de la pile d'entités hydrogéologiques - Mayotte | sa:PolyElementEH_MYT   |
| Entités hydrogéologiques de niveau 3 - Mayotte                         | sa:EntiteHydroGeol_MYT |

## Base de données CarTHAgE

[http://www.professionnels.ign.fr/bdcarthage](http://www.professionnels.ign.fr/bdcarthage): CarTHAgE est la base de données sur la CARtographie THématique des AGences de l'Eau et du ministère chargé de l'environnement

Service WFS: [http://services.sandre.eaufrance.fr/geo/eth_MYT](http://services.sandre.eaufrance.fr/geo/eth_MYT)

|     **Donnée**      |       **Couche**       |
| ------------------- | ---------------------- |
| Cours d'eau Mayotte | sa:CoursEau |
| Cours d'eau Mayotte de 10 à 25km | sa:CoursEau4 |
| Cours d'eau Mayotte de 5 à 10km | sa:CoursEau5 |
| Cours d'eau Mayotte inf. à 5km | sa:CoursEau6 |
| Plans d'eau Mayotte | sa:PlanEau |
| Régions hydrographiques Mayotte | sa:RegionHydro |
| Secteurs hydrographiques Mayotte | sa:SecteurHydro |
| Sous-secteurs hydrographiques Mayotte | sa:SousSecteurHydro |
| Tronçons hydrographiques Circulaire Mayotte | sa:TronconHydrographique |
| Zones hydrographiques Mayotte | sa:ZoneHydro |