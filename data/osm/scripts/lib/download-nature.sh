#!/bin/bash

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${BASE_DIR}/util.sh"

echo ""
echo "------------------------------------------------------------"
echo "- TAG nature"
echo "-----"
echo ""

fetch_all "['natural'='beach']" "natural_beach.geojson"
fetch_all "['natural'='bay']" "natural_bay.geojson"
fetch_all "['natural'='coastline']" "natural_coastline.geojson"
fetch_all "['natural'='peak']" "natural_peak.geojson"
fetch_all "['natural'='reef']" "natural_reef.geojson"
fetch_all "['natural'='sand']" "natural_sand.geojson"
fetch_all "['natural'='shoal']" "natural_shoal.geojson"
fetch_all "['natural'='spring']" "natural_spring.geojson"
fetch_all "['natural'='water']" "natural_water.geojson"
fetch_all "['natural'='wetland']" "natural_wetland.geojson"
fetch_all "['natural'='wetland']['wetland'='mangrove']" "natural_wetland_mangrove.geojson"
fetch_all "['natural'='wood']" "natural_wood.geojson"