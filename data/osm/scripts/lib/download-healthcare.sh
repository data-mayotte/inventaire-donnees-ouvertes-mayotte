#!/bin/bash

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${BASE_DIR}/util.sh"

echo ""
echo "------------------------------------------------------------"
echo "- TAG healthcare"
echo "-----"
echo ""

fetch_all "['healthcare'='clinic']" "healthcare_clinic.geojson"
fetch_all "['healthcare'='dentist']" "healthcare_dentist.geojson"
fetch_all "['healthcare'='hospital']" "healthcare_hospital.geojson"
fetch_all "['healthcare'='doctor']" "healthcare_doctor.geojson"
fetch_all "['healthcare']" "healthcare.geojson"