#!/bin/bash

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${BASE_DIR}/util.sh"

echo ""
echo "------------------------------------------------------------"
echo "- TAG admin_level"
echo "-----"
echo ""

fetch_all "['admin_level'='2']" "admin_level_2.geojson"
fetch_all "['admin_level'='4']" "admin_level_4.geojson"
fetch_all "['admin_level'='8']" "admin_level_8.geojson"