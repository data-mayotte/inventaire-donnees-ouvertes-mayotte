#!/bin/bash

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${BASE_DIR}/util.sh"

echo ""
echo "------------------------------------------------------------"
echo "- TAG historic"
echo "-----"
echo ""

fetch_all "['historic']" "historic.geojson"
fetch_all "['historic'='monument']" "historic_monument.geojson"
fetch_all "['historic'='ruins']" "historic_ruins.geojson"
fetch_all "['historic'='tomb']" "historic_tomb.geojson"
