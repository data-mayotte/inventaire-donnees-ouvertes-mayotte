#!/bin/bash

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${BASE_DIR}/util.sh"

echo ""
echo "------------------------------------------------------------"
echo "- TAG tourism"
echo "-----"
echo ""

fetch_all "['tourism'='viewpoint']" "tourism_viewpoint.geojson"
fetch_all "['tourism'='picnic_site']" "tourism_picnic_site.geojson"
fetch_all "['tourism'='museum']" "tourism_museum.geojson"
fetch_all "['tourism'='hotel']" "tourism_hotel.geojson"
fetch_all "['tourism'='hostel']" "tourism_hostel.geojson"
fetch_all "['tourism'='information']" "tourism_information.geojson"
fetch_all "['tourism']" "tourism.geojson"