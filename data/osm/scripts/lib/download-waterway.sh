#!/bin/bash

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${BASE_DIR}/util.sh"

echo ""
echo "------------------------------------------------------------"
echo "- TAG waterway"
echo "-----"
echo ""

fetch_all "['waterway']" "waterway.geojson"
fetch_all "['waterway'='river']" "waterway_river.geojson"
fetch_all "['waterway'='stream']" "waterway_stream.geojson"
fetch_all "['waterway'='waterfall']" "waterway_waterfall.geojson"
fetch_all "['waterway'='dam']" "waterway_dam.geojson"
fetch_all "['waterway'='drain']" "waterway_drain.geojson"