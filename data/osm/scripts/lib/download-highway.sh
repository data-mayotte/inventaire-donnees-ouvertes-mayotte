#!/bin/bash

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${BASE_DIR}/util.sh"

echo ""
echo "------------------------------------------------------------"
echo "- TAG highway"
echo "-----"
echo ""

fetch_all "['highway']" "highway.geojson"
fetch_all "['highway'='primary']" "highway_primary.geojson"
fetch_all "['highway'='primary_link']" "highway_primary_link.geojson"
fetch_all "['highway'='secondary']" "highway_secondary.geojson"
fetch_all "['highway'='secondary_link']" "highway_secondary_link.geojson"
fetch_all "['highway'='tertiary']" "highway_tertiary.geojson"
fetch_all "['highway'='tertiary_link']" "highway_tertiary_link.geojson"
fetch_all "['highway'='residential']" "highway_residential.geojson"
fetch_all "['highway'='footway']" "highway_footway.geojson"
fetch_all "['highway'='pedestrian']" "highway_pedestrian.geojson"
fetch_all "['highway'='path']" "highway_path.geojson"
fetch_all "['highway'='service']" "highway_service.geojson"
fetch_all "['highway'='steps']" "highway_steps.geojson"
fetch_all "['highway'='track']" "highway_track.geojson"
fetch_all "['highway'='unclassified']" "highway_unclassified.geojson"