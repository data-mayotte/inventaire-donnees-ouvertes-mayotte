#!/bin/bash

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${BASE_DIR}/util.sh"

echo ""
echo "------------------------------------------------------------"
echo "- TAG leisure"
echo "-----"
echo ""

fetch_all "['leisure']" "leisure.geojson"
fetch_all "['leisure'='park']" "leisure_park.geojson"
fetch_all "['leisure'='stadium']" "leisure_stadium.geojson"
fetch_all "['leisure'='sport-center']" "leisure_sport-center.geojson"
fetch_all "['leisure'='pitch']" "leisure_pitch.geojson"
fetch_all "['leisure'='garden']" "leisure_garden.geojson"
fetch_all "['leisure'='swimming_pool']" "leisure_swimming_pool.geojson"
fetch_all "['leisure'='track']" "leisure_track.geojson"