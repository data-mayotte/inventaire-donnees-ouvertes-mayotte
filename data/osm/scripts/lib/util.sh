#!/bin/bash

LIB_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SCRIPTS_DIR="$( cd "$( dirname "${LIB_DIR}" )" && pwd )"
DATA_OSM_DIR="$( cd "$( dirname "${SCRIPTS_DIR}" )" && pwd )"

DATA_OSM_OVERPASS_DIR="$DATA_OSM_DIR/overpass"
mkdir -p ${DATA_OSM_OVERPASS_DIR}

echo ""

MAYOTTE_GEOMETRY="(-13.3,44.7,-12.3,45.5)"

fetch_all () {
    echo "Download ${1} to $DATA_OSM_OVERPASS_DIR/${2}..."
    echo "[out:json][timeout:25];(way${1}${MAYOTTE_GEOMETRY};node${1}${MAYOTTE_GEOMETRY};relation${1}${MAYOTTE_GEOMETRY};);out;>;out skel qt;" | query-overpass --flat-properties > $DATA_OSM_OVERPASS_DIR/$2
}

