# Téléchargement des données OpenStreetMap

## Description

Les données d'OpenStreetMap ne sont pas stockées dans le projet afin de ne pas le surcharger.

Ce script permet de les télécharger grace au service [OverPass Turbo](http://overpass-turbo.eu/) d'OpenStreetMap.

Ce script fonctionne sous linux et mac.

## Pré-requis

Installer [NodeJS](https://nodejs.org/en/download/package-manager/).

Puis installer la librairie query-overpass via NPM:

```bash
npm install -g query-overpass
```

## Utilisation

Pour mettre à jour toutes les données:

```bash
./download-osm-data.sh
```

## Licence

![Public Domain](https://i.creativecommons.org/p/zero/1.0/88x31.png "Public Domain").

Ces scripts sont libres de droits (licence [CC0 1.0 universel (CC0 1.0)](https://creativecommons.org/publicdomain/zero/1.0/deed.fr)).