#!/bin/bash

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${SCRIPTS_DIR}/lib/util.sh"

echo ""
echo "############################################################"
echo "# DOWNLOAD OSM DATA - GEOMETRY: ${MAYOTTE_GEOMETRY}"
echo "#####"
echo ""

source "${SCRIPTS_DIR}/lib/download-admin_level.sh"
source "${SCRIPTS_DIR}/lib/download-amenity.sh"
source "${SCRIPTS_DIR}/lib/download-healthcare.sh"
source "${SCRIPTS_DIR}/lib/download-highway.sh"
source "${SCRIPTS_DIR}/lib/download-historic.sh"
source "${SCRIPTS_DIR}/lib/download-landuse.sh"
source "${SCRIPTS_DIR}/lib/download-leisure.sh"
source "${SCRIPTS_DIR}/lib/download-nature.sh"
source "${SCRIPTS_DIR}/lib/download-power.sh"
source "${SCRIPTS_DIR}/lib/download-shop.sh"
source "${SCRIPTS_DIR}/lib/download-tourism.sh"
source "${SCRIPTS_DIR}/lib/download-waterway.sh"





echo ""
echo "done"
echo ""
