# OpenStreetMap - données sur Mayotte

[https://www.openstreetmap.org](https://www.openstreetmap.org)

![Carte](../../qgis/provider/osm/screenshots/qgis-osm-mayotte-global2.png)

## Projets QGIS exemples

Voir [projets QGIS OpenStreetMap](../../qgis/provider/osm/README.md)

## Contours, limites, frontières administratives et naturelles (boundary, admin_level, natural)

![Carte](../../qgis/provider/osm/screenshots/qgis-osm-mayotte-limites.png)

Documentation:

- [https://wiki.openstreetmap.org/wiki/FR:Key:admin_level](https://wiki.openstreetmap.org/wiki/FR:Key:admin_level)
- [https://wiki.openstreetmap.org/wiki/FR:Tag:boundary%3Dadministrative](https://wiki.openstreetmap.org/wiki/FR:Tag:boundary%3Dadministrative)

Tutoriel:

- [Tracer les limites administratives](https://wiki.openstreetmap.org/wiki/WikiProject_France/Limites_administratives/Tracer_les_limites_administratives)

Overpass API:

|      **Donnée**      |                    **Lien**                    |  **Clé/valeur**   |
| -------------------- | ---------------------------------------------- | ----------------- |
| Limites nationales   | [overpass api](http://overpass-turbo.eu/s/B96) | admin_level=2     |
| Limites régionales   | [overpass api](http://overpass-turbo.eu/s/B95) | admin_level=4     |
| Limites communales   | [overpass api](http://overpass-turbo.eu/s/BBR) | admin_level=8     |
| Limites côtières     | [overpass api](http://overpass-turbo.eu/s/B8j) | natural=coastline |
| Limites des communes | [overpass api](http://overpass-turbo.eu/s/B8v) | admin_level=8     |
| Barrières de récif   | [overpass api](http://overpass-turbo.eu/s/B98) | natural=reef      |
| Zones humides        | [overpass api](http://overpass-turbo.eu/s/B9a) | natural=wetland   |

## Cours d'eau (waterway)

![Carte](../../qgis/provider/osm/screenshots/qgis-osm-mayotte-cours-d-eau.png)

Documentation:

- [https://wiki.openstreetmap.org/wiki/FR:Key:waterway](https://wiki.openstreetmap.org/wiki/FR:Key:waterway)
- [https://wiki.openstreetmap.org/wiki/FR:Relation:waterway](https://wiki.openstreetmap.org/wiki/FR:Relation:waterway)

## Usage des sols (landuse)

Documentation:

- [https://wiki.openstreetmap.org/wiki/FR:Key:landuse](https://wiki.openstreetmap.org/wiki/FR:Key:landuse)

## Routes (highway), Bâtiments (building)

![Carte](../../qgis/provider/osm/screenshots/qgis-osm-mayotte-routes-batiments.png)

Documentation: 

- [https://wiki.openstreetmap.org/wiki/FR:Key:highway](https://wiki.openstreetmap.org/wiki/FR:Key:highway)
- [https://wiki.openstreetmap.org/wiki/FR:Key:building](https://wiki.openstreetmap.org/wiki/FR:Key:building)

## Equipements (amenity), magasins (shop), loisirs (leisure)

![Carte](../../qgis/provider/osm/screenshots/qgis-osm-mamoudzou-centre.png)

Documentation: 

- [https://wiki.openstreetmap.org/wiki/FR:Key:amenity](https://wiki.openstreetmap.org/wiki/FR:Key:amenity)
- [https://wiki.openstreetmap.org/wiki/FR:Key:shop](https://wiki.openstreetmap.org/wiki/FR:Key:shop)
- [https://wiki.openstreetmap.org/wiki/FR:Key:leisure](https://wiki.openstreetmap.org/wiki/FR:Key:leisure)

## Liens utiles

[Toutes les données OSM concernant Mayotte](http://download.geofabrik.de/europe/france/mayotte.html)

[Styles pour QGIS](https://github.com/charleyglynn/OSM-Shapefile-QGIS-stylesheets)

## License

Les données sont fournies sous licence [ODbL](https://www.openstreetmap.org/copyright)