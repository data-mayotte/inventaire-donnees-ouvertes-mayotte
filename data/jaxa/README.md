# JAXA (Japan Aerospace Exploration Agency) - données sur Mayotte

[https://www.eorc.jaxa.jp](https://www.eorc.jaxa.jp)

## Modèle numérique de terrain

Résolution: 1 arc/second (30m)

Date: 2018

__Note__: le téléchargement des données nécessite de [créer un compte](https://www.eorc.jaxa.jp/ALOS/en/aw3d30/registration.htm) (gratuit).

- global: [https://www.eorc.jaxa.jp/ALOS/en/aw3d30/index.htm](https://www.eorc.jaxa.jp/ALOS/en/aw3d30/index.htm)
- mayotte: [https://www.eorc.jaxa.jp/ALOS/en/aw3d30/data/dl/download_v1804.htm?S015E045_S013E045](https://www.eorc.jaxa.jp/ALOS/en/aw3d30/data/dl/download_v1804.htm?S015E045_S013E045)

![Carte](./images/jaxa-mnt-mayotte.png)