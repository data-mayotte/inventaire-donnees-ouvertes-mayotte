# BRGM - QGIS - Mayotte

## Description

Ces projet QGIS permet de démontrer un exemple de représentation des données du BRGM.

![Carte](../../../data/brgm/images/brgm-risques-mayotte.png)

## Données

Les données sont décrites dans [données BRGM](../../../data/brgm/README.md)

## Installation

1) télécharger le projet (ou faites un git clone)
2) ouvrir les projets du dossier qgis/provider/brgm dans QGIS 3.

![Carte](../../../data/brgm/images/brgm-geologie-mayotte.png)

## Tutoriel QGIS 3D

[https://www.geodose.com/2018/02/dem-3d-visualization-qgis.html](https://www.geodose.com/2018/02/dem-3d-visualization-qgis.html)

## Licence

![Public Domain](https://i.creativecommons.org/p/zero/1.0/88x31.png "Public Domain").

Ce projet QGIS est fourni sous licence [CC0 1.0 universel (CC0 1.0)](https://creativecommons.org/publicdomain/zero/1.0/deed.fr).