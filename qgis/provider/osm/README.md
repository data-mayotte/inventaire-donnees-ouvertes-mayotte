# OpenStreetMap - QGIS - Mayotte

## Description

Ce projet QGIS permet de démontrer un exemple de représentation des données d'OpenStreetMap.

![Carte](./screenshots/qgis-osm-mayotte-global.png)

## Données

Les données sont décrites dans [données OpenStreetMap](../../../data/osm/README.md)

![Carte](./screenshots/qgis-osm-mamoudzou-centre.png)

## Installation

1) télécharger le projet (ou faites un git clone)
2) [installer les données OpenStreetMap](../../../data/osm/scripts/README.md)
3) [télécharger les icônes SVG additionnelles](../../icons/README.md)
4) ouvrir le projet qgis/provider/osm/qgis-osm-mayotte.qgz dans QGIS 3.

## Licence

![Public Domain](https://i.creativecommons.org/p/zero/1.0/88x31.png "Public Domain").

Ce projet QGIS est fourni sous licence [CC0 1.0 universel (CC0 1.0)](https://creativecommons.org/publicdomain/zero/1.0/deed.fr).