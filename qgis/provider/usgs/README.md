# USGS - QGIS - Mayotte

## Description

Ce projet QGIS permet de démontrer un exemple de représentation des données de l'USGS.

![Carte](../../../data/usgs/images/usgs-seismes-mayotte-2018.png)

## Données

Les données sont décrites dans [données USGS](../../../data/usgs/README.md)

## Installation

1) télécharger le projet (ou faites un git clone)
2) [installer les données USGS](../../../data/usgs/scripts/README.md)
3) ouvrir le projet qgis/provider/usgs/qgis-usgs-mayotte.qgz dans QGIS 3.

## Licence

![Public Domain](https://i.creativecommons.org/p/zero/1.0/88x31.png "Public Domain").

Ce projet QGIS est fourni sous licence [CC0 1.0 universel (CC0 1.0)](https://creativecommons.org/publicdomain/zero/1.0/deed.fr).