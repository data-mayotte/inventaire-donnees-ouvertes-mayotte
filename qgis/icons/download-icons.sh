#!/bin/bash

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

fetch () {
    DOWNLOAD_URL="${1}"
    ORIGINAL_FOLDER_NAME="${2}"
    FINAL_FOLDER_NAME="${3}"
    ORIGINAL_ICONS_DIR="${BASE_DIR}/${ORIGINAL_FOLDER_NAME}"
    FINAL_ICONS_DIR="${BASE_DIR}/${FINAL_FOLDER_NAME}"
    echo "Download ${ICONS_SET_NAME} icons from ${DOWNLOAD_URL} to ${FINAL_ICONS_DIR}..."
    rm -rf ${ORIGINAL_ICONS_DIR} 
    rm -rf ${FINAL_ICONS_DIR} 
    wget -qO- -O ${ORIGINAL_FOLDER_NAME}.zip ${DOWNLOAD_URL} && unzip ${ORIGINAL_FOLDER_NAME}.zip && rm ${ORIGINAL_FOLDER_NAME}.zip && mv ${ORIGINAL_ICONS_DIR} ${FINAL_ICONS_DIR}

}

fetch "https://codeload.github.com/gravitystorm/openstreetmap-carto/zip/master" "openstreetmap-carto-master"  "openstreetmap-carto" 
fetch "https://gitlab.com/gmgeo/osmic/-/archive/master/osmic-master.zip" "osmic-master" "osmic" 

echo ""
echo "done"
echo ""




# wget -qO- -O tmp.zip http://downloads.wordpress.org/plugin/akismet.2.5.3.zip && unzip tmp.zip && rm tmp.zip
