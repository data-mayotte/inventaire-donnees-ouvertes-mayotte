# Icônes

## Téléchargement

Télécharger les icônes SVG additionnelles manuellement:

- [OSM](https://codeload.github.com/gravitystorm/openstreetmap-carto/zip/master)
- [OSMic](https://gitlab.com/gmgeo/osmic/-/archive/master/osmic-master.zip)

Ou automatiquement via le script:

```bash
./download-icons.sh
```

## Import dans QGIS

Ensuite, les importer dans QGIS (menu options/système).