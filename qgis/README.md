# QGIS

## Description

Projets QGIS permettant de donner des exemples de représentation des données concernant Mayotte.

## Projets

- [OpenStreetMap](./provider/osm/README.md)
- [USGS](./provider/usgs/README.md)
- d'autres projets à venir prochainement...

## Licence

![Public Domain](https://i.creativecommons.org/p/zero/1.0/88x31.png "Public Domain").

Ces projets QGIS sont libres de droits (licence [CC0 1.0 universel (CC0 1.0)](https://creativecommons.org/publicdomain/zero/1.0/deed.fr)).