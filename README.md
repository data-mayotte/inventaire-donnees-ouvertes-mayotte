# Inventaire des données géographiques ouvertes sur Mayotte

Ce projet est un inventaire de données ouvertes et/ou géolocalisées concernant la zone géographique de Mayotte.

Ces données sont destinées à être exploitées via un logiciel SIG comme [QGIS](https://www.qgis.org).

## Etat d'avancement

Ce projet est actuellement en cours de réalisation, et sera complété progressivement.

Pour plus de détails, consulter le [ticket de suivi](https://gitlab.com/data-mayotte/inventaire-donnees-ouvertes-mayotte/issues/6)

## QGIS

Des projets QGIS sont fournis afin d'illustrer des exemples d'utilisation de ces données.

Par fournisseur de données:

- [BRGM](./qgis/provider/brgm/README.md)
- [OpenStreetMap](./qgis/provider/osm/README.md)
- [USGS](./qgis/provider/usgs/README.md)
- à suivre...

## Fournisseurs de données

- [BRGM](./data/brgm/README.md): Bureau de Recherches Géologiques et Minières
- [CartoMer - AFB](./data/cartomer-afb/README.md): agence Française pour la Biodiversité
- [Etalab](./data/etalab/README.md): mission d'ouverture et de partage des données publiques du gouvernement français
- [GBIF](./data/gbif/README.md): système mondial d'informations sur la biodiversité
- [Geo Littoral](./data/geo-littoral/README.md): traits de côte
- [Ifrecor](./data/ifrecor/README.md): initiative française pour les récifs coralliens
- [IGN](./data/ign/README.md): institut national de l'information géographique et forestière
- [INPN](./data/inpn/README.md): Inventaire National du Patrimoine Naturel
- [Jaxa](./data/jaxa/README.md): Modèle numérique de terrain de la Japan Aerospace Exploration Agency
- [OpenStreetMap](./data/osm/README.md): base de données géographiques collaborative
- [Sandre](./data/sandre/README.md): référentiel des données sur l'eau
- [Sextan Ifremer](./data/sextan-ifremer/README.md): séismes
- [SHOM](./data/shom/README.md): information géographique maritime et littorale de référence
- [USGS](./data/usgs/README.md): institut d'études géologiques des États-Unis

![Carte](./qgis/provider/osm/screenshots/qgis-osm-mayotte-global2.png)

## Licence

![Public Domain](https://i.creativecommons.org/p/zero/1.0/88x31.png "Public Domain").

Cet inventaire est libre de droits (licence [CC0 1.0 universel (CC0 1.0)](https://creativecommons.org/publicdomain/zero/1.0/deed.fr)).